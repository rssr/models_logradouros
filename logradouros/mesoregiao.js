const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;
const UF = require('./uf');

const GeoDataSchemaPolygon = require('../geo-data/polygon');
var MesoregiaoSchema = new Schema({
    nome: {
        type: String,
        maxlength: 80,
        required: true,
        index: true,
        unique: false,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    uf: {
        type: String,
        ref: 'ufs'
    },
    idIBGE: {
        type: String,
        index: true,
        unique: true,
        trim: true
    },
    // https://docs.mongodb.com/manual/geospatial-queries/
    geoData: GeoDataSchemaPolygon
}, {
    versionKey: false
});


MesoregiaoSchema.statics.findByIdIBGE = function (idIBGE) {
    return this.findOne({
        idIBGE
    }).cache();
}
MesoregiaoSchema.statics.findByNome = function (nome) {
    return this.findOne({
        nome
    }).cache();
}

const Mesoregiao = mongoose.model('Mesoregiao', MesoregiaoSchema, 'Mesoregioes');
module.exports = Mesoregiao;