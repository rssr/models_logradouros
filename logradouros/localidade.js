const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;

const GeoDataSchemaPolygon = require('../geo-data/multilinestring');

const LOCALIDADE = 'localidade';
const DISTRITO = 'distrito';
const SUB_DISTRITO = 'subdistrito';
const BAIRRO = 'bairro';

const TipoLocalidade = {
    LOCALIDADE,
    DISTRITO,
    SUB_DISTRITO,
    BAIRRO
}

const {
    cleanCEP,
    ajustaCEP
} = require('./cep');
const discriminatorKey = 'tipo';

var LocalidadeSchema = new Schema({
    nome: {
        type: String,
        maxlength: 80,
        required: true,
        index: true,
        unique: false,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    municipio: {
        type: Types.ObjectId,
        ref: 'Municipio'
    },
    microregiao: {
        type: Types.ObjectId,
        ref: 'microregiao'
    },
    mesoregiao: {
        type: Types.ObjectId,
        ref: 'mesoregiao'
    },
    cep: {
        type: String,
        required: false,
        index: true,
        match: /^[0-9]{2}[0-9]{3}[0-9]{3}$/,
        set: ajustaCEP
    },
    faixaCep: {
        inicial: {
            type: String,
            required: false,
            index: true,
            match: "/^[0-9]{2}[0-9]{3}[0-9]{3}$/",
            set: ajustaCEP
        },
        final: {
            type: String,
            required: false,
            index: true,
            match: "/^[0-9]{2}[0-9]{3}[0-9]{3}$/",
            set: ajustaCEP
        }
    },
    idIBGE: {
        type: String,
        index: {
            name: 'idIBGE',
            unique: true
        },
        trim: true
    },
    // https://docs.mongodb.com/manual/geospatial-queries/
    geoData: GeoDataSchemaPolygon
}, {
    discriminatorKey,

});

const DistritoSchema = new Schema({});

const SubDistritoSchema = new Schema({
    distrito: {
        type: Types.ObjectId,
        ref: 'Distrito'
    }
});
const BairroSchema = new Schema({});

SubDistritoSchema.index({
    idIBGE: 1,
    'distrito.idIBGE': 1
}, {
    name: 'idIBGE_distrito_subdistrito',
    unique: true
})


LocalidadeSchema.statics.findByIdIBGE =
    SubDistritoSchema.statics.findByIdIBGE =
    BairroSchema.statics.findByIdIBGE = function (idIBGE) {
        return this.findOne({
            idIBGE
        }).cache();
    }

LocalidadeSchema.statics.findByNome = function (nome, tipo) {
    if (!tipo)
        return this.findOne({
            nome
        }).cache();
    else
        return this.findOne({
            nome,
            tipo
        }).cache()
}

DistritoSchema.statics.findByNome =
    SubDistritoSchema.statics.findByNome =
    BairroSchema.statics.findByNome = function (nome) {
        return this.findOne({
            nome
        }).cache();
    }

const Localidade = mongoose.model('Localidade', LocalidadeSchema, 'Localidades');
const Distrito = Localidade.discriminator(DISTRITO, DistritoSchema);
const SubDistrito = Localidade.discriminator(SUB_DISTRITO, SubDistritoSchema);
const Bairro = Localidade.discriminator(BAIRRO, BairroSchema);

module.exports = {
    TipoLocalidade,
    Localidade,
    Distrito,
    SubDistrito,
    Bairro
};