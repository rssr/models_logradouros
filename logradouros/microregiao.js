const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;
const UF = require('./uf');
const GeoDataSchemaPolygon = require('../geo-data/polygon');

var MicroregiaoSchema = new Schema({
   nome: {
      type: String,
      maxlength: 80,
      required: true,
      index: true,
      unique: false,
      trim: true,
      collation: { locale: 'pt', strength: 1, caseLevel: false }
   },
   mesoregiao: {
      type: Types.ObjectId,
      ref: 'Mesoregioes'
   },
   uf: {
      type: String,
      ref: 'ufs'
   },
   idIBGE: {
      type: String,
      index: true,
      unique: true,
      trim: true
   },
   // https://docs.mongodb.com/manual/geospatial-queries/
   geoData: GeoDataSchemaPolygon
},{
   versionKey: false
   });

   
MicroregiaoSchema.statics.findByIdIBGE = function (idIBGE) { 
    return this.findOne({
       idIBGE
   }).cache();
}

MicroregiaoSchema.statics.findByNome = function (nome) {
    return this.findOne({
       nome
   }).cache();
}

const Microregiao = mongoose.model('Microregiao', MicroregiaoSchema,'Microregioes');
module.exports = Microregiao;