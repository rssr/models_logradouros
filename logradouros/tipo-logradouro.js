const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;

var TipoLogradouroSchema = new Schema({
      nome: {
            type: String,
            maxlength: 80,
            required: true,
            unique: true,
            trim: true,
            collation: { locale: 'pt', strength: 1, caseLevel: false }
      },
      abreviatura: {
            type: String,
            maxlength: 5,
            required: true,
            index: true,
            unique: true,
            trim: true
      },
      _id: {
            type: String,
            maxlength: 3,
            minlength: 3,
            trim: true,
            alias: 'tipo'
      }
}, {
      versionKey: false
});

TipoLogradouroSchema.index({
      nome: 1
}, {
      unique: true,
    //  collation: {
    //        locale: 'pt',
    //        strength: 2
    //  }
});

TipoLogradouroSchema.statics.findOneByAbreviatura = async function (abreviatura) {
      if (abreviatura) abreviatura = abreviatura.trim();
      return await this.findOne({
            abreviatura
      }).cache();
}
TipoLogradouroSchema.statics.findOneByNome = async function (nome) {
      if (nome) nome = nome.trim();
      
      return await this.findOne({
            'nome':nome
      }).cache()
      //.collation({ locale: 'pt', strength: 2 });
}
TipoLogradouroSchema.statics.desconhecido = function () {
      if (this._tipo_desconhecido) return this._tipo_desconhecido;
      return this.findById('UKN')//.cache()
            .then(tipo => {
                  return this._tipo_desconhecido = tipo;
            })
            .catch(err => {
                  console.log("Erro fatal ao obter o Tipo Desconhecido de Logradouro!");
                  console.log(err);
                  return null;
            })
}

const TipoLogradouro = mongoose.model('TipoLogradouro', TipoLogradouroSchema, 'TiposDeLogradouros');

(function () {
      console.log("Criando Tipos Padrões de Logradouros !")

      TipoLogradouro.create([{
            'tipo': 'NCL',
            'nome': 'Núcleo',
            'abreviatura': 'Ncl'
      }, {
            'tipo': 'PSG',
            'nome': 'Passagem',
            'abreviatura': 'Pas.'
      }, {
            'tipo': 'UKN',
            'nome': 'Desconhecido (Não identificado)',
            'abreviatura': '---'
      }, {
            "tipo": "CNJ",
            "nome": "Conjunto",
            "abreviatura": "Cj."
      }, {
            'nome': 'Alameda',
            'tipo': 'ALM',
            'abreviatura': 'Alm.'
      }, {
            'nome': 'Avenida',
            'tipo': 'AVN',
            'abreviatura': 'Av.'
      }, {
            'nome': 'Beco',
            'tipo': 'BEC',
            'abreviatura': 'B.'
      }, {
            'nome': 'Boulevard',
            'tipo': 'BLV',
            'abreviatura': 'Blv.'
      }, {
            'nome': 'Caminho',
            'tipo': 'CAM',
            'abreviatura': 'Cpn.'
      }, {
            'nome': 'Cais',
            'tipo': 'CAS',
            'abreviatura': 'Cais'
      }, {
            'nome': 'Campo',
            'tipo': 'CMP',
            'abreviatura': 'Cmp.'
      }, {
            'nome': 'Escada',
            'tipo': 'ESC',
            'abreviatura': 'Esc.'
      }, {
            'nome': 'Estrada',
            'tipo': 'ETR',
            'abreviatura': 'Est.'
      }, {
            'nome': 'Favela',
            'tipo': 'FAV',
            'abreviatura': 'Fav.'
      }, {
            'nome': 'Fazenda',
            'tipo': 'FAZ',
            'abreviatura': 'Faz.'
      }, {
            'nome': 'Floresta',
            'tipo': 'FLT',
            'abreviatura': 'Flr.'
      }, {
            'nome': 'Ilha',
            'tipo': 'ILH',
            'abreviatura': 'Ilha'
      }, {
            'nome': 'Jardim',
            'tipo': 'JRD',
            'abreviatura': 'Jrd.'
      }, {
            'nome': 'Ladeira',
            'tipo': 'LAD',
            'abreviatura': 'Ldr.'
      }, {
            'nome': 'Largo',
            'tipo': 'LRG',
            'abreviatura': 'Lrg.'
      }, {
            'nome': 'Loteamento',
            'tipo': 'LTM',
            'abreviatura': 'Ltm.'
      }, {
            'nome': 'Lugar',
            'tipo': 'LUG',
            'abreviatura': 'Lgr.'
      }, {
            'nome': 'Morro',
            'tipo': 'MRR',
            'abreviatura': 'Mr.'
      }, {
            'nome': 'Parque',
            'tipo': 'PQE',
            'abreviatura': 'Prq.'
      }, {
            'nome': 'Passeio',
            'tipo': 'PAS',
            'abreviatura': 'Pss.'
      }, {
            'nome': 'Praia',
            'tipo': 'PRA',
            'abreviatura': 'Praia'
      }, {
            'nome': 'Praça',
            'tipo': 'PRC',
            'abreviatura': 'Prç.'
      }, {
            'nome': 'Recanto',
            'tipo': 'REC',
            'abreviatura': 'Rc.'
      }, {
            'nome': 'Rodovia',
            'tipo': 'ROD',
            'abreviatura': 'Rd.'
      }, {
            'nome': 'Rua',
            'tipo': 'RUA',
            'abreviatura': 'R.'
      }, {
            'nome': 'Servidão',
            'tipo': 'SRV',
            'abreviatura': 'Svd.'
      }, {
            'nome': 'Travessa',
            'tipo': 'TRV',
            'abreviatura': 'Trv.'
      }, {
            'nome': 'Via',
            'tipo': 'VIA',
            'abreviatura': 'Via'
      }, {
            'nome': 'Vila',
            'tipo': 'VIL',
            'abreviatura': 'Vl.'
      }, {
            'nome': 'Viela',
            'tipo': 'VIE',
            'abreviatura': 'Vi.'
      }, {
            'nome': 'Sitio',
            'tipo': 'STI',
            'abreviatura': 'St.'
      }]).then(r => {
            console.log(r);

      }).catch(err => {
            if (err.code == 11000) return Promise.resolve();
            console.log("erro fatal ao salvar tipo de logradouro!");
            console.log(err);
            process.exit(1);
      }).then(
            TipoLogradouro.desconhecido()
      );
})();



module.exports = TipoLogradouro;