function ajustaCEP(cep) {
   if (!cep) return '00000000';
   else if (cep.length < 8)
      cep = cep + `0`.repeat(cep.length - 8);
   return cep;
}

function cleanCEP(cep) {
   return ajustaCEP(cep.toString().replace(/(\.)|(\-)/g, ''));
}
const CEPFormatRegex = /^[0-9]{2}.[0-9]{3}-[0-9]{3}$/;
const CEPregex = /(^[0-9]{2}[0-9]{3}-[0-9]{3}$)|(^[0-9]{2}.[0-9]{3}-[0-9]{3}$)|(^[0-9]{2}[0-9]{3}[0-9]{3}$)/
module.exports = {
   ajustaCEP,
   cleanCEP,
   CEPFormatRegex,
   CEPregex
}