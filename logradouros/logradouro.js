const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;

const GeoDataSchemaMultilineString = require('../geo-data/multilinestring');

let TIPO_CEP = [];
TIPO_CEP.EXCLUSIVO = 'EXCLUSIVO';
TIPO_CEP.FAIXA = 'FAIXA';
TIPO_CEP.REGIAO = 'REGIAO';

const {cleanCEP,ajustaCEP,CEPregex} = require('./cep');
var LogradouroSchema = new Schema({
    tipo: {
        type: String,
        ref: 'TipoLogradouro',
        index: true,
        required: true
    },
    nome: {
        type: String,
        maxlength: 80,
        required: true,
        index: true,
        unique: false,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    nomesAlternativos:[{
        type: String,
        maxlength: 80,
        required: true,
        index: true,
        unique: false,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    }],
    localidade: {
        type: Types.ObjectId,
        ref: 'localidades'
    },
    uf: {
        type: String,
        ref: 'ufs'
    },
    municipio: {
        type: Types.ObjectId,
        ref: 'municipios'
    },
    codMunicipioTemporario: Number,
    tipoCEP: {
        type: String,
        enum: [TIPO_CEP.EXCLUSIVO, TIPO_CEP.FAIXA, TIPO_CEP.REGIAO],
        default: TIPO_CEP.EXCLUSIVO
    },
    faixa: {
        inicio: {
            type: Number
        },
        fim: {
            type: Number
        }
    },
    cep: {
        type: String,
        required: false,
        index: {
            name: 'cep',
            unique: false // pode haver CEPS por regiões portanto vários logradouros terão o mesmo CEP.
        },
        match: CEPregex,
        default: "00000000",
        set: ajustaCEP
    },
    // https://docs.mongodb.com/manual/geospatial-queries/
    geoData: GeoDataSchemaMultilineString
}, {
    versionKey: false
});

LogradouroSchema.statics.findByCep = function (cep) {
    cep = cleanCEP(cep);
    return this.find({
            cep
        }).cache()
        .catch(err => {
            console.log("Error em pesquisar Logradouro pelo CEP");
            console.log(err);
            return null;
        });
}

const Logradouro = mongoose.model('Logradouro', LogradouroSchema, 'Logradouros');

module.exports = {
    Logradouro,
    TIPO_CEP
}