const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;
const UF = require('./uf');
const {
    ajustaCEP
} = require('./cep');
const GeoDataSchemaPolygon = require('../geo-data/polygon');
var MunicipioSchema = new Schema({
    nome: {
        type: String,
        maxlength: 80,
        required: true,
        index: true,
        unique: false,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    apelido: {
        type: String,
        maxlength: 80,
        index: true,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    capital: {
        type: Boolean
    },
    populacao: {
        type: Number
    },
    microregiao: {
        type: Types.ObjectId,
        ref: 'microregiao'
    },
    mesoregiao: {
        type: Types.ObjectId,
        ref: 'mesoregiao'
    },
    uf: {
        type: String,
        ref: 'ufs'
    },
    idIBGE: {
        type: String,
        index: true,
        unique: true,
        trim: true
    },
    cep: {
        type: String,
        required: false,
        index: true,
        match: "/^[0-9]{2}[0-9]{3}[0-9]{3}$/",
        set: ajustaCEP
    },
    faixaCep: {
        inicial: {
            type: String,
            required: false,
            index: true,
            match: "/^[0-9]{2}[0-9]{3}[0-9]{3}$/",
            set: ajustaCEP
        },
        final: {
            type: String,
            required: false,
            index: true,
            match: "/^[0-9]{2}[0-9]{3}[0-9]{3}$/",
            set: ajustaCEP
        }
    },
    // https://docs.mongodb.com/manual/geospatial-queries/
    geoData: GeoDataSchemaPolygon
}, {
    versionKey: false
});

MunicipioSchema.statics.findByCEP = function (cep) {
    if (!cep) return null;
    if (cep.length != 8)
        cep = cep + `0`.repeat(8 - cep.length);
    return this.findOne({
        $or: [{
            'faixaCep.cepInicial': {
                $eq: cep
            }
        }, {
            'faixaCep.final': {
                $eq: cep
            }
        }, {
            $and: [{
                'faixaCep.cepInicial': {
                    $gt: cep
                },
                'faixaCep.final': {
                    $lt: cep
                }

            }]
        }]
    })
}
MunicipioSchema.statics.findByIdIBGE = function (idIBGE) {
    return this.findOne({
        idIBGE
    }).cache();
}

MunicipioSchema.statics.findByNome = function (nome) {
    return this.findOne({
        nome
    }).cache();
}

const Municipio = mongoose.model('Municipio', MunicipioSchema, 'Municipios');
module.exports = Municipio;