const {
   TipoLocalidade,
   Localidade,
   SubDistrito,
   Distrito,
   Bairro
} = require('./localidade');

const {
   Logradouro,
   TIPO_CEP
} = require('./logradouro');

module.exports = {
   UF: require('./uf'),
   Municipio: require('./municipio'),
   TipoLocalidade,
   Localidade,
   Distrito,
   SubDistrito,
   Bairro,
   TipoLogradouro: require('./tipo-logradouro'),
   Logradouro,
   TIPO_CEP,

}