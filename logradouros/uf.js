const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;

var UFSchema = new Schema({
    _id: {
        type: String,
        maxlength: 2,
        alias: 'sigla',
        uppercase: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    nome: {
        type: String,
        maxlength: 80,
        required: true,
        index: true,
        unique: false,
        trim: true,
        collation: { locale: 'pt', strength: 1, caseLevel: false }
    },
    populacao: {
        type: Number
    },
    idIBGE: {
        type: Number,
        index: true,
        unique: true,
        get: v => Math.round(v),
        set: v => Math.round(v),
    },
    flagFile: {
        type: String
    }
}, {
    versionKey: false
});


UFSchema.statics.findByIdIBGE = function (idIBGE) {
    idIBGE = Math.round(idIBGE);
    return this.findOne({
        idIBGE
    });
}

UFSchema.statics.findByNome = function (nome) {
    if (!nome)
        return;
    nome = nome.trim();

    return this.findOne({
        nome
    })
    //.collation({
    //    locale: 'pt',
    //    strength: 2
    //});;
}
UFSchema.statics.findBySigla = function (sigla) {
    if (!sigla)
        return;
    sigla = sigla.trim();

    return this.findById(
        sigla
    )
    //.collation({
    //    locale: 'pt',
    //    strength: 2
    //});;
}

const UF = mongoose.model('UF', UFSchema, 'UFs');
module.exports = UF;